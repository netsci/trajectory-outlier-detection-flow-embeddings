{
 "cells": [
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# Measurements on Synthetic Experiments\n",
    "\n",
    "In this notebook, we use the synthetic examples from `01-Synthetic-Preparation.ipynb` to evaluate our approach in terms of accuracy to find the outlier trajectory. You must execute the other notebook first to generate the examples."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 1,
   "metadata": {},
   "outputs": [],
   "source": [
    "import multiprocessing\n",
    "import pickle\n",
    "from itertools import chain\n",
    "from pathlib import Path\n",
    "from typing import Tuple\n",
    "\n",
    "import numpy as np\n",
    "import matplotlib.pyplot as plt\n",
    "import seaborn as sns\n",
    "from sklearn.ensemble import IsolationForest\n",
    "from sklearn.metrics import accuracy_score\n",
    "from sklearn.mixture import GaussianMixture\n",
    "from sklearn.model_selection import train_test_split, GridSearchCV\n",
    "from sklearn.neighbors import LocalOutlierFactor\n",
    "from tqdm import tqdm\n",
    "\n",
    "from util import lexsort_rows\n",
    "from util.drawing import plot_gaussian_mixture\n",
    "from util.graph import edges_on_path\n",
    "from util.incidence import create_node_edge_incidence_matrix, create_triangle_list, create_edge_triangle_incidence_matrix\n",
    "from util.trajectory import full_trajectory_matrix, flatten_trajectory_matrix, harmonic_projection_matrix, create_matrix_coordinates_trajectory_Hspace"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 2,
   "metadata": {},
   "outputs": [],
   "source": [
    "base_dir = Path('.') / 'cache' / 'synthetic'\n",
    "num_graphs = 100\n",
    "\n",
    "# graph parameters\n",
    "number_points = 1000\n",
    "number_holes = 2\n",
    "\n",
    "# trajectory parameters\n",
    "num_trajectory_classes = 3\n",
    "num_trajectories_per_class = 40\n",
    "num_outliers = 5\n",
    "\n",
    "# plots\n",
    "sns.set_theme()\n",
    "trajectory_color_palette = sns.color_palette(n_colors=num_trajectory_classes + 1)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Load Synthetic Examples"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 3,
   "metadata": {},
   "outputs": [
    {
     "name": "stderr",
     "output_type": "stream",
     "text": [
      "Reading Graphs: 100%|██████████| 100/100 [00:00<00:00, 154.51it/s]\n"
     ]
    }
   ],
   "source": [
    "graphs = []\n",
    "graph_trajectories = []\n",
    "\n",
    "elists = []\n",
    "\n",
    "for i in tqdm(range(num_graphs), desc='Reading Graphs'):\n",
    "    storage_dir = base_dir / f'n{number_points}_rng{i}'\n",
    "\n",
    "    with (storage_dir / 'graph.pickle').open('rb') as file:\n",
    "        graphs.append(pickle.load(file))\n",
    "    with (storage_dir / 'trajectories.pickle').open('rb') as file:\n",
    "        graph_trajectories.append(pickle.load(file))\n",
    "\n",
    "    elists.append(lexsort_rows(np.array(graphs[i].edges())))"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Preparation"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Compute Hodgle Laplacians"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 4,
   "metadata": {},
   "outputs": [
    {
     "name": "stderr",
     "output_type": "stream",
     "text": [
      "Constructing Hodgle Laplacians: 100%|██████████| 100/100 [00:02<00:00, 41.41it/s]\n"
     ]
    }
   ],
   "source": [
    "hodgle_laplacians = []\n",
    "\n",
    "for graph, elist in tqdm(zip(graphs, elists), desc='Constructing Hodgle Laplacians', total=len(graphs)):\n",
    "    B1 = create_node_edge_incidence_matrix(elist)\n",
    "    B2 = create_edge_triangle_incidence_matrix(elist, create_triangle_list(graph))\n",
    "\n",
    "    hodgle_laplacians.append(B1.T @ B1 + B2 @ B2.T)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Compute Harmonic Projection Matrices"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 5,
   "metadata": {},
   "outputs": [
    {
     "name": "stderr",
     "output_type": "stream",
     "text": [
      "Computing Projection Matrices: 100%|██████████| 100/100 [01:14<00:00,  1.35it/s]\n"
     ]
    }
   ],
   "source": [
    "def harmonic_projections(hodgle_laplacian) -> np.ndarray:\n",
    "    return harmonic_projection_matrix(hodgle_laplacian.astype(float), number_holes)\n",
    "\n",
    "iterator = map(harmonic_projections, hodgle_laplacians)\n",
    "projection_matrices = list(tqdm(iterator, desc='Computing Projection Matrices', total=num_graphs))"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Compute Trajectory Embeddings"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Full Trajectories"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 6,
   "metadata": {},
   "outputs": [
    {
     "name": "stderr",
     "output_type": "stream",
     "text": [
      "Computing Projections: 100%|██████████| 100/100 [00:01<00:00, 58.44it/s]\n"
     ]
    }
   ],
   "source": [
    "full_trajectory_matrices = []\n",
    "full_trajectory_embeddings = []\n",
    "\n",
    "def full_trajectory_projection(parameters):\n",
    "    graph, elist, trajectories, projection_matrix = parameters\n",
    "    elist_dict = {(j[0], j[1]): i for i, j in enumerate(graph.edges())}\n",
    "\n",
    "    M_full = full_trajectory_matrix(graph, map(lambda path: list(edges_on_path(path)), chain.from_iterable(trajectories)), elist, elist_dict)\n",
    "    mat_coord_Hspace = create_matrix_coordinates_trajectory_Hspace(projection_matrix, M_full)\n",
    "\n",
    "    return M_full, mat_coord_Hspace\n",
    "\n",
    "with multiprocessing.Pool() as pool:\n",
    "    iterator = pool.imap(full_trajectory_projection, zip(graphs, elists, graph_trajectories, projection_matrices))\n",
    "    for full_trajectory_matrix, full_trajectory_embedding in tqdm(iterator, desc='Computing Projections', total=num_graphs):\n",
    "        full_trajectory_matrices.append(full_trajectory_matrix)\n",
    "        full_trajectory_embeddings.append(full_trajectory_embedding)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 7,
   "metadata": {},
   "outputs": [
    {
     "name": "stderr",
     "output_type": "stream",
     "text": [
      "Plotting Embeddings: 100%|██████████| 100/100 [00:06<00:00, 16.08it/s]\n"
     ]
    }
   ],
   "source": [
    "def plot_full_embedding(parameters: Tuple[int, np.ndarray]) -> None:\n",
    "    j, embeddings = parameters\n",
    "    fig, ax = plt.subplots(figsize=(8, 6))\n",
    "\n",
    "    ax.set_title('Projection of the flatten trajectories in the harmonic space')\n",
    "    ax.set_xlabel(r'Projection onto $\\bf{u_{harm}^{(1)}}$')\n",
    "    ax.set_ylabel(r'Projection onto $\\bf{u_{harm}^{(2)}}$')\n",
    "\n",
    "    cumsums = list(map(lambda i: [np.cumsum(i[0]), np.cumsum(i[1])], embeddings))\n",
    "    for i, cumsum in enumerate(cumsums):\n",
    "        color = trajectory_color_palette[i // num_trajectories_per_class]\n",
    "        sns.lineplot(x=cumsum[0], y=cumsum[1], color=color, ax=ax, sort=False)\n",
    "\n",
    "    fig.savefig(base_dir / f'n{number_points}_rng{j}' / 'full_embeddings.pdf', bbox_inches='tight')\n",
    "    plt.close(fig)\n",
    "\n",
    "with multiprocessing.Pool() as pool:\n",
    "    iterator = pool.imap_unordered(plot_full_embedding, enumerate(full_trajectory_embeddings))\n",
    "    list(tqdm(iterator, desc='Plotting Embeddings', total=num_graphs))"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Flatten Trajectories"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 8,
   "metadata": {},
   "outputs": [
    {
     "name": "stderr",
     "output_type": "stream",
     "text": [
      "Computing Projections: 100%|██████████| 100/100 [00:00<00:00, 282.52it/s]\n"
     ]
    }
   ],
   "source": [
    "def flatten_trajectory_projection(parameters) -> None:\n",
    "    full_trajectory_matrix, projection_matrix = parameters\n",
    "    M_flatten = flatten_trajectory_matrix(full_trajectory_matrix)\n",
    "    return [projection_matrix @ mat for mat in M_flatten]\n",
    "\n",
    "with multiprocessing.Pool() as pool:\n",
    "    iterator = pool.imap(flatten_trajectory_projection, zip(full_trajectory_matrices, projection_matrices))\n",
    "    trajectory_embeddings = list(tqdm(iterator, desc='Computing Projections', total=num_graphs))\n",
    "\n",
    "# iterator = map(flatten_trajectory_projection, zip(full_trajectory_matrices, projection_matrices))\n",
    "# trajectory_embeddings = list(tqdm(iterator, desc='Computing Projections', total=num_graphs))"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 9,
   "metadata": {},
   "outputs": [
    {
     "name": "stderr",
     "output_type": "stream",
     "text": [
      "Plotting Embeddings: 100%|██████████| 100/100 [00:04<00:00, 21.34it/s]\n"
     ]
    }
   ],
   "source": [
    "def plot_flatten_embedding(parameters: Tuple[int, np.ndarray]) -> None:\n",
    "    j, embeddings = parameters\n",
    "    fig, ax = plt.subplots(figsize=(8, 6))\n",
    "\n",
    "    ax.set_title('Projection of the flatten trajectories in the harmonic space')\n",
    "    ax.set_xlabel(r'Projection onto $\\bf{u_{harm}^{(1)}}$')\n",
    "    ax.set_ylabel(r'Projection onto $\\bf{u_{harm}^{(2)}}$')\n",
    "\n",
    "    for i, flatten_trajectory_embedding in enumerate(embeddings):\n",
    "        color = trajectory_color_palette[i // num_trajectories_per_class]\n",
    "        ax.scatter(flatten_trajectory_embedding[0], flatten_trajectory_embedding[1], color=color)\n",
    "\n",
    "    fig.savefig(base_dir / f'n{number_points}_rng{j}' / 'embeddings.pdf', bbox_inches='tight')\n",
    "    plt.close(fig)\n",
    "\n",
    "with multiprocessing.Pool() as pool:\n",
    "    iterator = pool.imap_unordered(plot_flatten_embedding, enumerate(trajectory_embeddings))\n",
    "    list(tqdm(iterator, desc='Plotting Embeddings', total=num_graphs))"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Classification of Outliers"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 10,
   "metadata": {},
   "outputs": [],
   "source": [
    "contamination_factor = num_outliers / (num_trajectory_classes * num_trajectories_per_class + num_outliers)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Local Outlier Factor"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 11,
   "metadata": {},
   "outputs": [
    {
     "name": "stderr",
     "output_type": "stream",
     "text": [
      "Computing Local Outlier Factors: 100%|██████████| 100/100 [00:19<00:00,  5.13it/s]\n"
     ]
    },
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "Accuracy: 0.97544\n"
     ]
    }
   ],
   "source": [
    "def fit_model(parameters: Tuple[int, np.ndarray]) -> bool:\n",
    "    i, embeddings = parameters\n",
    "\n",
    "    embeddings = np.array(embeddings)\n",
    "    ground_truth = np.ones(embeddings.shape[0], dtype=int)\n",
    "    ground_truth[-num_outliers:] = -1\n",
    "\n",
    "    clf = LocalOutlierFactor(contamination=contamination_factor)\n",
    "    prediction = clf.fit_predict(embeddings)\n",
    "\n",
    "    fig, ax = plt.subplots(figsize=(8, 6))\n",
    "\n",
    "    ax.set_title('Outlier Scores of the Harmonic Projections')\n",
    "    ax.set_xlabel(r'Projection onto $\\bf{u_{harm}^{(1)}}$')\n",
    "    ax.set_ylabel(r'Projection onto $\\bf{u_{harm}^{(2)}}$')\n",
    "\n",
    "    ax.scatter(embeddings[prediction == 1, 0], embeddings[prediction == 1, 1], color='g', s=3., label='Classified as Normal')\n",
    "    ax.scatter(embeddings[prediction != 1, 0], embeddings[prediction != 1, 1], color='r', s=3., label='Classified as Abnormal')\n",
    "\n",
    "    # Only plot the outlier score if it is not too small. Otherwise it will merge with the data point\n",
    "    # itself and interpreting the color of the point will become hard.\n",
    "    radius = (clf.negative_outlier_factor_.max() - clf.negative_outlier_factor_) / (clf.negative_outlier_factor_.max() - clf.negative_outlier_factor_.min())\n",
    "    points_with_outlier_score = abs(radius) > 0.007\n",
    "    ax.scatter(embeddings[points_with_outlier_score, 0], embeddings[points_with_outlier_score, 1], s=1000 * radius[points_with_outlier_score], edgecolors='b', facecolors='none', label='Outlier scores')\n",
    "\n",
    "    legend = ax.legend(loc='best')\n",
    "    legend.legendHandles[0]._sizes = [10]\n",
    "    legend.legendHandles[1]._sizes = [10]\n",
    "    legend.legendHandles[2]._sizes = [30]\n",
    "\n",
    "    fig.savefig(base_dir / f'n{number_points}_rng{i}' / 'local_outlier_factor.pdf', bbox_inches='tight')\n",
    "    plt.close(fig)\n",
    "\n",
    "    return accuracy_score(ground_truth, prediction)\n",
    "\n",
    "with multiprocessing.Pool(processes=1) as pool:\n",
    "    iterator = pool.imap_unordered(fit_model, enumerate(trajectory_embeddings))\n",
    "    results = list(tqdm(iterator, desc='Computing Local Outlier Factors', total=num_graphs))\n",
    "print('Accuracy:', np.mean(results))"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Fit a Gaussian Mixture Model\n",
    "\n",
    "1. For each synthetic example, split the 'real' trajectories (excluding outliers) into a train and a test set.\n",
    "2. Perform hyper-parameter tuning to find the optimal number of gaussians to fit a gaussian mixture model to the training set.\n",
    "3. Validate the resulting mixture model on the outliers. They should have small probability to belonging to any of the gaussians."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 12,
   "metadata": {},
   "outputs": [
    {
     "name": "stderr",
     "output_type": "stream",
     "text": [
      "Fitting Gaussian Mixture Model to Embeddings: 100%|██████████| 100/100 [00:34<00:00,  2.91it/s]"
     ]
    },
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "Correct: 0.0\n"
     ]
    },
    {
     "name": "stderr",
     "output_type": "stream",
     "text": [
      "\n"
     ]
    }
   ],
   "source": [
    "def fit_model(parameters: Tuple[int, np.ndarray]) -> bool:\n",
    "    i, embeddings = parameters\n",
    "    embeddings = np.array(embeddings)\n",
    "\n",
    "    real_trajectory_embeddings = embeddings[0:-num_outliers]\n",
    "    train_embeddings, test_embeddings = train_test_split(real_trajectory_embeddings, test_size=0.1, random_state=i)\n",
    "\n",
    "    # add the outlier to the test embeddings\n",
    "    test_embeddings = np.vstack((test_embeddings, embeddings[-1]))\n",
    "    test_labels = np.full(len(test_embeddings), 0)\n",
    "    test_labels[-num_outliers] = 1\n",
    "\n",
    "    gaussian_mixture = GaussianMixture(covariance_type='full')\n",
    "    gaussian_mixture = GridSearchCV(gaussian_mixture, {'n_components': [2, 3, 4, 5, 6, 7]})\n",
    "    gaussian_mixture.fit(train_embeddings)\n",
    "\n",
    "    fig, ax = plt.subplots(figsize=(8, 6))\n",
    "    ax.set_title('Projection of the flatten trajectories in the harmonic space')\n",
    "    ax.set_xlabel(r'Projection onto $\\bf{u_{harm}^{(1)}}$')\n",
    "    ax.set_ylabel(r'Projection onto $\\bf{u_{harm}^{(2)}}$')\n",
    "\n",
    "    plot_gaussian_mixture(gaussian_mixture.best_estimator_, embeddings, gaussian_mixture.predict(embeddings), colors=trajectory_color_palette, ax=ax)\n",
    "    fig.savefig(base_dir / f'n{number_points}_rng{i}' / 'gaussian_mixture.pdf', bbox_inches='tight')\n",
    "    plt.close(fig)\n",
    "\n",
    "    scores = gaussian_mixture.score_samples(test_embeddings)\n",
    "    if np.allclose(scores <= 0, test_labels):\n",
    "        return True\n",
    "    return False\n",
    "\n",
    "with multiprocessing.Pool(processes=1) as pool:\n",
    "    iterator = pool.imap_unordered(fit_model, enumerate(trajectory_embeddings))\n",
    "    results = list(tqdm(iterator, desc='Fitting Gaussian Mixture Model to Embeddings', total=num_graphs))\n",
    "print('Correct:', sum(results) / num_graphs)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Isolation Forest"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 13,
   "metadata": {},
   "outputs": [
    {
     "name": "stderr",
     "output_type": "stream",
     "text": [
      "Fitting Isolation Forests: 100%|██████████| 100/100 [00:39<00:00,  2.55it/s]"
     ]
    },
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "Accuracy: 0.9892\n"
     ]
    },
    {
     "name": "stderr",
     "output_type": "stream",
     "text": [
      "\n"
     ]
    }
   ],
   "source": [
    "from matplotlib.colors import LinearSegmentedColormap\n",
    "\n",
    "rwth_blue = {\n",
    "    0.00: '#ffffff',\n",
    "    0.10: '#e1e7e4',\n",
    "    0.25: '#bac7da',\n",
    "    0.50: '#809dc3',\n",
    "    0.75: '#4e7caf',\n",
    "    1.00: '#0f65a0'\n",
    "}\n",
    "\n",
    "rwth_blue_colormap = LinearSegmentedColormap.from_list(\n",
    "    'rwth_blue', list(rwth_blue.items())).reversed()\n",
    "\n",
    "def fit_model(parameters: Tuple[int, np.ndarray]) -> bool:\n",
    "    i, embeddings = parameters\n",
    "    embeddings = np.array(embeddings)\n",
    "\n",
    "    normal_embeddings = embeddings[0:-num_outliers]\n",
    "    outlier_embeddings = embeddings[-num_outliers:]\n",
    "\n",
    "    ground_truth = np.ones(embeddings.shape[0], dtype=int)\n",
    "    ground_truth[-num_outliers:] = -1\n",
    "\n",
    "    isolation_forest = IsolationForest(contamination=contamination_factor, random_state=i)\n",
    "    isolation_forest.fit_predict(embeddings)\n",
    "\n",
    "    fig, ax = plt.subplots(figsize=(8, 6))\n",
    "\n",
    "    # Plot the embedding points first. This should populate the axes limits\n",
    "    # properly and allows us to set the grid for the background contour.\n",
    "    ax.scatter(normal_embeddings[:, 0], normal_embeddings[:, 1], c='#57AB27', s=10, edgecolor='k', label='Regular Observations')\n",
    "    ax.scatter(outlier_embeddings[:, 0], outlier_embeddings[:, 1], c='#CC071E', s=10, edgecolor='k', label='Abnormal Observations')\n",
    "\n",
    "    xx, yy = np.meshgrid(np.linspace(*ax.get_xlim(), 50), np.linspace(*ax.get_ylim(), 50))\n",
    "    Z = isolation_forest.decision_function(np.c_[xx.ravel(), yy.ravel()])\n",
    "    Z = Z.reshape(xx.shape)\n",
    "\n",
    "    ax.set_title(\"IsolationForest\")\n",
    "    ax.set_xlabel(r'Projection onto $\\bf{u_{harm}^{(1)}}$')\n",
    "    ax.set_ylabel(r'Projection onto $\\bf{u_{harm}^{(2)}}$')\n",
    "\n",
    "    ax.contourf(xx, yy, Z, cmap=rwth_blue_colormap, zorder=-10)\n",
    "\n",
    "    ax.legend(loc='best')\n",
    "\n",
    "    fig.savefig(base_dir / f'n{number_points}_rng{i}' / 'isolation_forest.pdf', bbox_inches='tight')\n",
    "    plt.close(fig)\n",
    "\n",
    "    return accuracy_score(ground_truth, isolation_forest.predict(embeddings))\n",
    "\n",
    "with multiprocessing.Pool(processes=1) as pool:\n",
    "    iterator = pool.imap_unordered(fit_model, enumerate(trajectory_embeddings))\n",
    "    results = list(tqdm(iterator, desc='Fitting Isolation Forests', total=num_graphs))\n",
    "print('Accuracy:', np.mean(results))"
   ]
  }
 ],
 "metadata": {
  "interpreter": {
   "hash": "1380a7290b89bdd4f722aca5eef1199eac25d2732e4f96a662d440084536b140"
  },
  "kernelspec": {
   "display_name": "Python 3.8.10 64-bit ('venv': venv)",
   "name": "python3"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.8.10"
  },
  "orig_nbformat": 4
 },
 "nbformat": 4,
 "nbformat_minor": 2
}
