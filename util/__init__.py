from typing import List, Tuple, Union

import networkx as nx
import numpy as np
from shapely.affinity import rotate, scale
from shapely.geometry import Point

SEED = Union[int, np.random.RandomState, None]


def lexsort_rows(array: np.ndarray) -> np.ndarray:
    array = np.array(array)
    return array[np.lexsort(np.rot90(array))]


def make_ellipse(center: Point, semi_major: float, semi_minor: float, rotation: float):
    circle = center.buffer(1.0)
    ellipse_base = scale(circle, semi_major, semi_minor)
    return rotate(ellipse_base, rotation)


def random_point_in_exterior(corridor_size: float = 0.2, *, seed=None) -> np.ndarray:
    """
    Selects a random point in the exterior of the 2-dimensional space
    [0, 1] ⨉ [0, 1].
    """
    random_state = nx.utils.create_random_state(seed)

    x = random_state.uniform(0.0, corridor_size)
    y = random_state.uniform(0.0, 1.0)

    if random_state.uniform() < 0.5:
        x, y = x, y
    if random_state.uniform() < 0.5:
        x = 1.0 - x
        y = 1.0 - y

    return np.array((x, y))


def random_pair_in_exterior(corridor_size: float = 0.2, existing_locations: List[np.ndarray] = None, *, seed=None) -> Tuple[np.ndarray, np.ndarray]:
    random_state = nx.utils.create_random_state(seed)
    if existing_locations is None:
        existing_locations = []

    while True:
        departure = random_point_in_exterior(corridor_size, seed=random_state)
        arrival = random_point_in_exterior(corridor_size, seed=random_state)

        # Ensure some distance between departure and arrival area. This should lead to more
        # interesting trajectories.
        if np.linalg.norm(departure - arrival) < 0.6:
            continue

        # Ensure that the departure and arrival area is not very similar to the departure and
        # arrival area of another trajectory class. This would mean that the two classes are
        # actually just one class, which is particularly problematic of one of the classes is
        # the outlier.
        found_similar_class = False
        for existing in existing_locations:
            if np.linalg.norm(existing[0] - departure) < 0.3 and np.linalg.norm(existing[1] - arrival) < 0.3:
                found_similar_class = True
                break
        if found_similar_class:
            continue

        # if this is reached, we found a good departure and arrival location pair
        return departure, arrival
