from typing import List, Optional

import contextily as ctx
import matplotlib.pyplot as plt
import networkx as nx
import numpy as np
import seaborn as sns
from matplotlib.collections import PatchCollection
from matplotlib.colors import LinearSegmentedColormap
from matplotlib.patches import Polygon
from sklearn.ensemble import IsolationForest
from sklearn.exceptions import NotFittedError
from sklearn.neighbors import LocalOutlierFactor
from sklearn.utils.validation import check_is_fitted

from util.graph import edges_on_path, sort_corners_of_hole
from util.hexagonal_world import HexagonalWorld

rwth_blue_colormap = LinearSegmentedColormap.from_list(
    'rwth_blue',
    [(0.00, '#ffffff'), (0.10, '#e1e7e4'), (0.25, '#bac7da'), (0.50, '#809dc3'), (0.75, '#4e7caf'), (1.00, '#0f65a0')]
)

def set_axis_limits(ax: plt.Axes, hexagonal_world: HexagonalWorld, *, padding: float = 0.) -> None:
    if hexagonal_world.restriction is not None:
        bounds = hexagonal_world.restriction.bounds
    else:
        bounds = [-180, -90, 180, 90]

    ax.set_xlim(max(bounds[0] - padding, -180), min(bounds[2] + padding, 180))
    ax.set_ylim(max(bounds[1] - padding, -90), min(bounds[3] + padding, 90))


def init_world_axis(ax: plt.Axes, hexagonal_world: HexagonalWorld, crs: str, *, padding: float = 0.) -> None:
    set_axis_limits(ax, hexagonal_world, padding=padding)
    ctx.add_basemap(ax, crs=crs, attribution=False)


def plot_hexagons_on_map(ax, hexagonal_world: HexagonalWorld, **kwargs) -> None:
    """
    Plots the hexagons defined by `hexagonal_world` on the given axis.

    For best results, the axis should be limited to the restriction of the
    hexagonal world.
    """
    for hexagon in hexagonal_world.hexagons:
        x = [point.x for point in hexagonal_world.layout.polygon_corners(hexagon)]
        y = [point.y for point in hexagonal_world.layout.polygon_corners(hexagon)]
        ax.plot(x + [x[0]], y + [y[0]], color='#787878', **kwargs)


def plot_isolation_forest(isolation_forest: IsolationForest, points: Optional[np.ndarray] = None, *,
                          ax: Optional[plt.Axes] = None) -> None:
    """
    Plots a 2-D point cloud with the isolation forest's decision function as background.

    You *must* plot all other data before using this function, as this function takes the current
    limits for the x- and y-axis as range of the contour that illustrates the decision boundaries
    of the isolation forest.

    Parameters
    ----------
    isolation_forest : sklearn.ensemble.IsolationForest
        The isolation forest instance. The classifier can be pre-fitted such that
        `isolation_forest.decision_function()` is available. Otherwise the estimator is
        automatically fitted on `points` if available.
    points : np.ndarray of size m ⨉ 2, default=None
        If given, the points are plotted as 2-D point cloud on top of the contours of the isolation
        forest. You may which to do this yourself to have greater flexibility of the plotting
        behavior.
    ax : matplotlib.pyplot.Axes, default=None
        The axis to which the isolation forest should be plotted. If not provided, this function
        automatically plots to the current axis of the current figure as given by
        `matplotlib.pyplot.gca()`.
    """
    ax = ax or plt.gca()

    try:
        check_is_fitted(isolation_forest)
    except:
        if points is None:
            raise ValueError('Either the isolation forest must be pre-fitted or a collection of '
                             'points must be given to fit the estimator.')
        else:
            isolation_forest.fit(points)

    # Plot the embedding points first. This should populate the axes limits properly and allows us
    # to set the grid for the background contour.
    if points is not None:
        predictions = isolation_forest.predict(points)
        point_colors = ['#57AB27' if pred > 0 else '#CC071E' for pred in predictions]
        ax.scatter(points[:, 0], points[:, 1], c=point_colors, edgecolor='#000000')

    # Divide the canvas into a grid with size 0.1 and compute the anomaly score for each point of
    # the grid. This should give a good resolution for the contour, but you can change the quality
    # by increasing or decreasing the `0.1` in the following line.
    step_size = 0.1
    x_coordinates, y_coordinates = np.meshgrid(
        np.arange(ax.get_xlim()[0], ax.get_xlim()[1] + step_size, step_size),
        np.arange(ax.get_ylim()[0], ax.get_ylim()[1] + step_size, step_size)
    )
    anomaly_scores = isolation_forest.decision_function(
        np.c_[x_coordinates.ravel(), y_coordinates.ravel()]
    ).reshape(x_coordinates.shape)

    # Plot the contour with a negative z-order such that it is in the background. We have to plot
    # the contour at the end because we require that the axis dimensions do not change anymore and
    # thus that all artists have been added to the figure already.
    ax.contourf(x_coordinates, y_coordinates, anomaly_scores, cmap=rwth_blue_colormap,
                zorder=-10)


def plot_local_outlier_factor(lof: LocalOutlierFactor, points: np.ndarray, *,
                              ax: Optional[plt.Axes] = None) -> None:
    """
    Plots a 2-D point cloud with the individual points' negative outlier factor as additional
    border (larger border equals higher outlier factor).

    Notice that the given local outlier factor instance must be unfitted! This is because this
    method needs to get the predictions for the given points, but local outlier factor instances
    can only compute predictions once during fitting.

    Parameters
    ----------
    lof : LocalOutlierFactor
        The untrained local outlier factor instance.
    points : np.ndarray of size m ⨉ 2
    ax : matplotlib.pyplot.Axes, default=None
        The axis to which the isolation forest should be plotted. If not provided, this function
        automatically plots to the current axis of the current figure as given by
        `matplotlib.pyplot.gca()`.
    """
    ax = ax or plt.gca()

    try:
        check_is_fitted(lof)
        raise ValueError('Given local outlier factor instance is prefitted. Can only work with '
                         'unfitted estimators.')
    except NotFittedError:
        pass

    predictions = lof.fit_predict(points)
    point_colors = ['#57AB27' if pred > 0 else '#CC071E' for pred in predictions]
    ax.scatter(points[:, 0], points[:, 1], c=point_colors, s=10)

    # Only plot the outlier score if it is not too small. Otherwise it will merge with the data
    # point itself and interpreting the color of the point will become hard.
    score_bandwidth = lof.negative_outlier_factor_.max() - lof.negative_outlier_factor_.min()
    radius = (lof.negative_outlier_factor_.max() - lof.negative_outlier_factor_) / score_bandwidth
    points_with_outlier_score = abs(radius) > 0.007

    ax.scatter(points[points_with_outlier_score, 0], points[points_with_outlier_score, 1],
               s=1000 * radius[points_with_outlier_score], edgecolors='#000000', facecolors='none')


def plot_network(graph: nx.Graph, node_positions: np.ndarray, holes: List[List[int]] = None, trajectory_classes: List[List] = None, hole_centers=None, *, ax: Optional[plt.Axes] = None) -> None:
    ax = ax or plt.gca()

    color_palette = sns.color_palette()

    try:
        if holes is not None:
            patches = []
            for hole in holes:
                corners = sort_corners_of_hole(graph, hole)
                corners = np.array(
                    list(map(lambda point: node_positions[point], corners)))
                patches.append(Polygon(corners, facecolor='#9c9e9f', closed=True))

            ax.add_collection(PatchCollection(
                patches, alpha=0.7, match_original=True))
    except RuntimeError:
        print('Error with graph')

    nx.draw(graph, node_positions, ax=ax, node_size=5,
            node_color='#646567', edge_color='#9c9e9f', with_labels=False)

    if trajectory_classes is not None:
        for class_id, trajectories in enumerate(trajectory_classes):
            if len(trajectories) > 1:
                color = color_palette[class_id]
            else:
                color = color_palette[-1]  # outlier

            for trajectory in trajectories:
                # We use a bigger node size here so that the arrows do not
                # fully reach the nodes. This makes the visualization a bit
                # better.
                nx.draw_networkx_edges(graph, node_positions, ax=ax, edgelist=list(
                    edges_on_path(trajectory)), node_size=10, width=2, edge_color=color, arrows=True, arrowstyle='->')

    if hole_centers is not None:
        ax.scatter(x=hole_centers[:, 0], y=hole_centers[:, 1], marker='x')
