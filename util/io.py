import pickle
from typing import Tuple

import geopandas

from util.hexagonal_world import HexagonalWorld


def read_dataset(path: str) -> Tuple[HexagonalWorld, geopandas.GeoDataFrame]:
    with open(path, 'rb') as file:
        data = pickle.load(file)
        return data['hexagonal_world'], data['dataframe']


def write_dataset(path: str, hexagonal_world: HexagonalWorld, dataframe: geopandas.GeoDataFrame) -> None:
    data = {
        'hexagonal_world': hexagonal_world,
        'dataframe': dataframe
    }

    with open(path, 'wb') as file:
        pickle.dump(data, file)
