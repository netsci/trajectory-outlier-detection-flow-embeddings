from copy import deepcopy
from typing import Generator, List, Tuple

import networkx as nx
import numpy as np
from scipy.sparse import csr_matrix, linalg

from util import SEED, random_pair_in_exterior
from util.graph import edges_on_path, point_to_closest_node


def random_trajectory_class(graph: nx.Graph, node_positions: np.ndarray, number_trajectories: int,
                            *, existing_locations: List[np.ndarray] = None, seed: SEED = None) -> Tuple[np.ndarray, np.ndarray, List[List[int]]]:
    random_state = nx.utils.create_random_state(seed)

    # We operate on a copy since we will alter the edge attributes below after each trajectory generation.
    # This change is only temporary and should not affect the other trajectory classes or the rest of the
    # computation.
    graph = deepcopy(graph)

    departure_point, arrival_point = random_pair_in_exterior(existing_locations=existing_locations, seed=random_state)
    departure_node = point_to_closest_node(departure_point, graph, node_positions, ignore_nodes=graph.graph['removed_nodes'])
    arrival_node = point_to_closest_node(arrival_point, graph, node_positions, ignore_nodes=graph.graph['removed_nodes'])

    possible_departures = list(graph.neighbors(departure_node)) + [departure_node]
    possible_arrivals = list(graph.neighbors(arrival_node)) + [arrival_node]

    trajectories = []
    for _ in range(number_trajectories):
        departure = random_state.choice(possible_departures)
        arrival = random_state.choice(possible_arrivals)

        trajectory = nx.shortest_path(graph, departure, arrival, weight='weight')

        trajectories.append(trajectory)

        # Increase the edge weights along the trajectory a bit. This should make it more
        # likely that the shortest path is slightly different in the next iteration.
        # This should generate distinct but similar trajectories between the departure and the
        # arrival node.
        for e0, e1 in edges_on_path(trajectory):
            graph[e0][e1]['weight'] += 0.0001

    return departure_point, arrival_point, trajectories


def interpolate_trajectory(trajectory: List[int], graph: nx.Graph) -> Generator[int, None, None]:
    previous_index = trajectory[0]
    yield previous_index

    for current_index in trajectory[1:]:
        if graph.has_edge(previous_index, current_index):
            yield current_index
            previous_index = current_index
        else:
            shortest_path: List[int] = nx.shortest_path(graph, previous_index, current_index)
            yield from shortest_path[1:]
            previous_index = shortest_path[-1]


def full_trajectory_matrix(graph: nx.Graph, mat_traj, elist, elist_dict) -> List[csr_matrix]:
    mat_vec_e = []
    for count, j in enumerate(mat_traj):
        if len(j) == 0:
            print(f"{count}: No Trajectory")
            continue

        data = []
        row_ind = []
        col_ind = []

        for i, (x, y) in enumerate(j):
            assert (x, y) in elist or (y, x) in elist  # TODO
            assert graph.has_edge(x, y)

            if x < y:
                idx = elist_dict[(x, y)]
                row_ind.append(idx)
                col_ind.append(i)
                data.append(1)
            if y < x:
                idx = elist_dict[(y, x)]
                row_ind.append(idx)
                col_ind.append(i)
                data.append(-1)

        mat_temp = csr_matrix((data, (row_ind, col_ind)), shape=(
            elist.shape[0], len(j)), dtype=np.int8)
        mat_vec_e.append(mat_temp)
    return mat_vec_e


def flatten_trajectory_matrix(H_full) -> np.ndarray:
    # TODO: This should return a sparse matrix
    flattened = map(lambda mat_tmp: mat_tmp.sum(axis=1), H_full)
    return np.array(list(flattened)).squeeze()


def create_matrix_coordinates_trajectory_Hspace(H, M_full):
    return [H @ mat for mat in M_full]


def harmonic_projection_matrix(L1: csr_matrix, number_of_holes: int) -> np.ndarray:
    """
    Coputes the harmonic projection matrix for the simplicial complex with the
    given Hodge-1 Laplacian.

    Parameters
    ----------
    L1 : csr_matrix of type float
    number_of_holes : int
    """
    _, v = linalg.eigsh(L1, k=number_of_holes,
                        v0=np.ones(L1.shape[0]), which='SM')
    return v.T
