from itertools import chain
from typing import Iterable, List, Optional, Set, Tuple, TypeVar

import networkx as nx
import numpy as np
from scipy.spatial import Delaunay
from shapely.geometry.point import Point

from util import make_ellipse, SEED
from util.holes import random_holes

V = TypeVar('V')


def edges_on_path(path: List[V]) -> Iterable[Tuple[V, V]]:
    return zip(path, path[1:])


def point_to_closest_node(point: np.ndarray, graph: nx.Graph, node_positions: np.ndarray, *, ignore_nodes: Optional[Set[int]] = None) -> int:
    distances = np.linalg.norm(node_positions - point, axis=1)
    for index in distances.argsort():
        if index not in ignore_nodes:
            return index
    raise RuntimeError('Could not find a closed node in the graph. Is the graph empty or did you ignore all nodes?')


def random_triangle_graph(number_points: int, *, seed: SEED = None,
                          distance_weights: bool = False) -> Tuple[nx.Graph, np.ndarray]:
    random_state: np.random.RandomState = nx.utils.create_random_state(seed)
    points = random_state.uniform(0, 1, (number_points, 2))
    tri = Delaunay(points)

    nodes = set()
    edges = set()

    for u, v, w in tri.simplices:
        nodes.update([u, v, w])
        edges.add(tuple(sorted([u, v])))
        edges.add(tuple(sorted([v, w])))
        edges.add(tuple(sorted([w, u])))

    graph = nx.Graph()
    graph.add_nodes_from(sorted(nodes))
    graph.add_edges_from(sorted(edges))

    if distance_weights:
        for a, b in graph.edges():
            graph[a][b]['weight'] = np.linalg.norm(points[a] - points[b])

    return graph, points


def random_graph(number_nodes: int, number_holes: int, *, seed=None) -> Tuple[nx.Graph, np.ndarray, List[List[int]], np.ndarray]:
    random_state = nx.utils.create_random_state(seed)

    graph, node_positions = random_triangle_graph(number_nodes, seed=random_state, distance_weights=True)
    graph.graph['removed_nodes'] = set()

    holes = []

    hole_centers, axis_values, hole_rotations = random_holes(number_holes, seed=random_state)

    for center, axis_value, hole_rotation in zip(hole_centers, axis_values, hole_rotations):
        center = Point(center)
        ellipse = make_ellipse(center, axis_value[1], axis_value[0], hole_rotation)

        removed_points_round = set()
        # TODO: this could be done way more efficiently
        for node_id, point in enumerate(map(lambda p: Point(*p), node_positions)):
            if ellipse.contains(point):
                removed_points_round.add(node_id)

        all_neighbors = set(chain.from_iterable(graph.neighbors(point) for point in removed_points_round))
        graph.remove_nodes_from(removed_points_round)
        graph.graph['removed_nodes'].update(removed_points_round)

        # Sometimes a node on the border of the hole remains that has only one neighbor
        # since all other neighbors have been deleted. We delete these nodes as well, as
        # they otherwise cause problems later on.
        hole = []
        for node in all_neighbors - removed_points_round:
            if graph.degree(node) > 1:
                hole.append(node)
            else:
                graph.remove_node(node)
                graph.graph['removed_nodes'].add(node)

        holes.append(hole)

    return graph, node_positions, holes, hole_centers


def sort_corners_of_hole_old(graph: nx.Graph, corners: List[int]) -> List[int]:
    for i in range(0, len(corners) - 1):
        neighbor_indexes = list(filter(lambda j: graph.has_edge(
            corners[i], corners[j]), range(i, len(corners))))

        # if there are two choices, use the one that only connects to the other
        # neighbor and the current node itself.
        if len(neighbor_indexes) == 1 or i == 0 or i == len(corners) - 2:
            chosen_neighbor_index = neighbor_indexes[0]
        elif len(neighbor_indexes) == 2:
            neighbors_0 = set(filter(lambda j: graph.has_edge(
                corners[neighbor_indexes[0]], corners[j]), range(i, len(corners))))
            print(neighbors_0)

            if neighbors_0 == {i, neighbor_indexes[1]}:
                chosen_neighbor_index = neighbor_indexes[0]
            else:
                chosen_neighbor_index = neighbor_indexes[1]
        else:

            raise RuntimeError('Cannot order corners of polygon.')

        corners[i + 1], corners[chosen_neighbor_index] = corners[chosen_neighbor_index], corners[i + 1]

    return corners


def sort_corners_of_hole(graph: nx.Graph, corners: List[int]) -> List[int]:
    def sort_corners_of_hole_inner(i: int, graph: nx.Graph, corners: List[int]) -> List[int]:
        if i == len(corners) - 1:
            if graph.has_edge(corners[0], corners[-1]):
                return corners
            raise RuntimeError('Solution is invalid.')

        neighbor_indexes = list(filter(lambda j: graph.has_edge(
            corners[i], corners[j]), range(i, len(corners))))

        for possible_neighbor in neighbor_indexes:
            corners[i + 1], corners[possible_neighbor] = corners[possible_neighbor], corners[i + 1]
            try:
                return sort_corners_of_hole_inner(i + 1, graph, corners)
            except RuntimeError:
                corners[i + 1], corners[possible_neighbor] = corners[possible_neighbor], corners[i + 1]

        raise RuntimeError('Exceeded all possible choices.')

    return sort_corners_of_hole_inner(0, graph, corners)
