import math
from dataclasses import dataclass
from typing import Generator, Iterable, List, Literal, Set, Tuple, Union

import networkx as nx
import numpy as np
from shapely.geometry import Point, Polygon


@dataclass(frozen=True)
class Hex:
    q: int
    r: int
    s: int

    def __post_init__(self) -> None:
        # Normally we would rely on static type-checking for this, but it is
        # highly critical that these parameters are correct. Otherwise we have
        # all sorts of strange errors thoughout the application.
        if not isinstance(self.q, int):
            raise ValueError(f'Parameter `q` must be of type int, got {type(self.q)}')
        if not isinstance(self.r, int):
            raise ValueError(f'Parameter `r` must be of type int, got {type(self.r)}')
        if not isinstance(self.s, int):
            raise ValueError(f'Parameter `s` must be of type int, got {type(self.s)}')

        if self.q + self.r + self.s != 0:
            raise ValueError(f'q + r + s must be 0. Values were {self.q}, {self.r} and {self.s}.')

    @classmethod
    def from_fractional(cls, q: float, r: float, s: float) -> 'Hex':
        """
        Constructs a new `Hex` object from fractional `q`, `r` and `s` values.

        See https://www.redblobgames.com/grids/hexagons/#rounding for further explanation.
        """
        qi = round(q)
        ri = round(r)
        si = round(s)

        q_diff = abs(qi - q)
        r_diff = abs(ri - r)
        s_diff = abs(si - s)

        if q_diff > r_diff and q_diff > s_diff:
            qi = -ri - si
        elif r_diff > s_diff:
            ri = -qi - si
        else:
            si = -qi - ri

        return cls(qi, ri, si)

    def neighbor(self, direction: int) -> 'Hex':
        """
        Returns the neighboring hexagon in the given `direction`.

        Warning: This method does not handle 'wrap-arounds' like on the world
        map. Use `HexagonalWorld.neighbor_of_hex()` instead.
        """
        hex_directions = [Hex(1, 0, -1), Hex(1, -1, 0), Hex(0, -1, 1),
                          Hex(-1, 0, 1), Hex(-1, 1, 0), Hex(0, 1, -1)]
        return self + hex_directions[direction]

    def __str__(self) -> str:
        return f'Hex({self.q}, {self.r}, {self.s})'

    def __add__(self, other) -> 'Hex':
        if type(self) is type(other):
            return Hex(self.q + other.q, self.r + other.r, self.s + other.s)
        return NotImplemented

    def __mul__(self, other) -> 'Hex':
        if isinstance(other, int):
            return Hex(self.q * other, self.r * other, self.s * other)
        return NotImplemented

    def _rmul__(self, other) -> 'Hex':
        return self.__mul__(other)

    def __sub__(self, other) -> 'Hex':
        if type(self) is type(other):
            return Hex(self.q - other.q, self.r - other.r, self.s - other.s)
        return NotImplemented


OrientationParam = Union['Orientation', Literal['pointy', 'flat']]


@dataclass(frozen=True)
class Orientation:
    """
    Class that defines the orientation of the hexagons, i.e., how they are
    rotated and arranged in a grid.

    Standard orientations are flat topped and pointy topped hexagons, which are
    available through the methods `Orientation.flat_orientation()` and
    `Orientation.flat_orientation()`, respectively.
    """
    f0: float
    f1: float
    f2: float
    f3: float
    b0: float
    b1: float
    b2: float
    b3: float
    start_angle: float

    @classmethod
    def pointy_orientation(cls) -> 'Orientation':
        """
        Returns the standard pointy orientation, i.e,. where we have a corner
        at the top and at the bottom and edges on the left and right.
        """
        return cls(math.sqrt(3.0), math.sqrt(3.0) / 2.0, 0.0, 3.0 / 2.0, math.sqrt(3.0) / 3.0, -1.0 / 3.0, 0.0, 2.0 / 3.0, 0.5)

    @classmethod
    def flat_orientation(cls) -> 'Orientation':
        """
        Returns the standard flat orientation, i.e., where we have edges at the
        top and at the bottom and a corner on the left and right.
        """
        return cls(3.0 / 2.0, 0.0, math.sqrt(3.0) / 2.0, math.sqrt(3.0), 2.0 / 3.0, 0.0, -1.0 / 3.0, math.sqrt(3.0) / 3.0, 0.0)

    @classmethod
    def from_orientation_param(cls, param: OrientationParam) -> 'Orientation':
        """
        Helper method that either just passes through a given `Orientation`
        object, or constructs a standard pointy or flat orientation if either
        `pointy` or `flat` is given as string literal.
        """
        if isinstance(param, cls):
            return param

        if isinstance(param, str):
            if param == 'pointy':
                return Orientation.pointy_orientation()
            if param == 'flat':
                return Orientation.flat_orientation()
            raise ValueError(f'Given orientation `{param}` is unknown.')

        raise ValueError('Cannot construct orientation object from given orientation parameter.')


@dataclass(frozen=True)
class Layout:
    orientation: Orientation
    size: Tuple[float, float]
    origin: Point

    def hex_to_pixel(self, h: Hex) -> Point:
        x = (self.orientation.f0 * h.q + self.orientation.f1 * h.r) * self.size[0]
        y = (self.orientation.f2 * h.q + self.orientation.f3 * h.r) * self.size[1]
        return Point(x + self.origin.x, y + self.origin.y)

    def pixel_to_hex(self, p: Point) -> Hex:
        pt = Point((p.x - self.origin.x) / self.size[0], (p.y - self.origin.y) / self.size[1])
        q = self.orientation.b0 * pt.x + self.orientation.b1 * pt.y
        r = self.orientation.b2 * pt.x + self.orientation.b3 * pt.y
        return Hex.from_fractional(q, r, -q - r)

    def hex_corner_offset(self, corner: int) -> Point:
        angle = 2.0 * math.pi * (self.orientation.start_angle - corner) / 6.0
        return Point(self.size[0] * math.cos(angle), self.size[1] * math.sin(angle))

    def polygon_corners(self, h: Hex) -> Iterable[Point]:
        center = self.hex_to_pixel(h)
        for i in range(6):
            offset = self.hex_corner_offset(i)
            yield Point(center.x + offset.x, center.y + offset.y)

    def polygon(self, h: Hex) -> Polygon:
        return Polygon(self.polygon_corners(h))

    def __hash__(self) -> int:
        return hash(self.orientation) ^ hash((self.size[0], self.size[1], self.origin.x, self.origin.y))


class WorldLayout(Layout):
    def pixel_to_hex(self, p: Point) -> Hex:
        hex = super().pixel_to_hex(p)
        center_point = self.hex_to_pixel(hex)
        normalized_center = Point(center_point.x, center_point.y)
        return super().pixel_to_hex(normalized_center)

    def polygon_corners(self, h: Hex) -> Generator[Point, None, None]:
        center = self.hex_to_pixel(h)
        for i in range(6):
            offset = self.hex_corner_offset(i)
            yield Point(center.x + offset.x, center.y + offset.y)
