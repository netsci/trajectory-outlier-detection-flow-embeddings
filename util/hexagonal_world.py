import pickle
from itertools import product
from pathlib import Path
from typing import Generator, Optional, Set, Tuple

import geopandas
import networkx as nx
import numpy as np
from shapely import affinity, speedups
from shapely.geometry import MultiPolygon, Point, Polygon, box

from util.hexagon import (Hex, Layout, Orientation, OrientationParam,
                          WorldLayout)


class HexagonalWorld:
    cache_dir: Optional[Path]
    hexagons: Set[Hex]

    def __init__(self, layout: WorldLayout, restriction: Optional[Polygon] = None, cache_dir: Optional[Path] = None) -> None:
        self.layout = layout
        self.restriction = restriction

        self.cache_dir = cache_dir
        if self.cache_dir is not None:
            self.cache_dir.mkdir(parents=True, exist_ok=True)

        self._construct_all_hexagons()

    def __hash__(self) -> int:
        if self.restriction is None:
            return hash(self.layout) ^ hash(None)

        # The restriction polygon is completely defined by its exterior
        # coordinates and all the coordinates of its interiors. Cross-reference
        # https://github.com/Toblerity/Shapely/pull/985 and following as to why
        # Shapely's data classes are currently not hashable.
        final_hash = hash(self.layout) ^ hash(tuple(self.restriction.exterior.coords))
        for interior in self.restriction.interiors:
            final_hash ^= hash(tuple(interior.coords))
        return final_hash

    @classmethod
    def for_layout_parameters(cls, orientation: OrientationParam, size: Point, restriction: Optional[Polygon] = None) -> 'HexagonalWorld':
        orientation = Orientation.from_orientation_param(orientation)
        layout = WorldLayout(orientation, size, Point(0, 0))
        return cls(layout, restriction)

    def networkx_graph(self) -> nx.Graph:
        graph = nx.Graph()
        graph.add_nodes_from(self.hexagons)
        graph.add_edges_from(self.edge_list())
        return graph

    def edge_list(self) -> Generator[Tuple[Hex, Hex], None, None]:
        """
        Returns a list of all 'edges' (adjacent hexagons) in the grid.
        """
        for hexagon in list(self.hexagons):
            for _, neighbor in self.neighbors_of_hex(hexagon):
                # We see the same edge from the other node as well. This check
                # ensures uniqueness for the returned edges.
                if self.hex_to_index[hexagon] > self.hex_to_index[neighbor]:
                    continue

                yield hexagon, neighbor

    def incidence_matrix(self):
        return nx.incidence_matrix(self.networkx_graph())

    def neighbor_of_hex(self, hex: Hex, direction: int) -> Optional[Hex]:
        """
        Returns the normalized neighbor of the given hexagon in the specified
        direction.

        The coordinates wrap around on the borders of the world projection. In
        case the requested neighbor is outside the restriction boundary, `None`
        is returned.
        """
        neighbor = hex.neighbor(direction)
        normalized_neighbor = self.normalize_hex(neighbor)
        if normalized_neighbor in self.hexagons:
            return normalized_neighbor
        return None

    def neighbors_of_hex(self, hex: Hex) -> Generator[Tuple[int, Hex], None, None]:
        for direction in range(6):
            neighbor = self.neighbor_of_hex(hex, direction)
            if neighbor is not None:
                yield direction, neighbor

    def normalize_hex(self, hex: Hex) -> Hex:
        """
        Normalize the hexagon such that in the case a hexagon is partially
        outside of the world, it is mapped to the corresponding hexagon at the
        other end of the world.

        Outside of the world in this context refers to when a coordinate is
        outside of the [-180, +180] range for longitude or [-90, +90] range for
        latitude part.

        This way, a hexagon that partially covers the world somewhere in the
        -180-longitude part is equal to the hexagon that partially covers the
        world on the +180-longitude part, while the latitude part is kept.
        """
        center_point = self.layout.hex_to_pixel(hex)
        normalized_center = Point(center_point.x, center_point.y)
        return self.layout.pixel_to_hex(normalized_center)

    def coordinate_to_hex(self, coordinate: Point) -> Hex:
        """
        Returns the normalized hexagon that contains the given coordinate.
        """
        hex = self.layout.pixel_to_hex(coordinate)
        return self.normalize_hex(hex)

    def _construct_all_hexagons(self) -> None:
        cache_file = (self.cache_dir /
                      'hexagons.pickle') if self.cache_dir else None
        if cache_file is not None and cache_file.exists():
            with cache_file.open('rb') as f:
                self.hexagons = pickle.load(f)
        else:
            self.hexagons = set()

            if self.restriction is None:
                left_lon, top_lat, right_lon, bottom_lat = -180, -90, 180, 90
            else:
                left_lon, top_lat, right_lon, bottom_lat = self.restriction.bounds

            step_size = min(*self.layout.size) / 3

            # Constructs all `Hex` objects that are within the given restriction
            # bound. This method is rather brute-force, but it does the job just
            # fine and is not too time-consuming. For larger maps (or smaller
            # hexagons), we may need to optimize this part a bit.
            hexagons = set()
            all_long = np.arange(left_lon, right_lon + step_size, step_size)
            all_lat = np.arange(top_lat, bottom_lat + step_size, step_size)
            all_coordinates = product(all_long, all_lat)
            for lon, lat in all_coordinates:
                point = Point(lon, lat)
                hex = self.coordinate_to_hex(point)
                # The call to `_coordinate_in_restriction` can be very costly. As
                # such, we guarantee that it is not called when the hexagon is
                # already in the list.
                if hex not in hexagons and self._coordinate_in_restriction(point):
                    hexagons.add(hex)

            for hexagon in hexagons:
                if self._hexagon_in_restriction(hexagon):
                    self.hexagons.add(hexagon)

            if cache_file is not None:
                with cache_file.open('wb') as f:
                    pickle.dump(self.hexagons, f)

        self.hex_to_index = {hex: i for i, hex in enumerate(self.hexagons)}
        self.index_to_hex = {i: hex for i, hex in enumerate(self.hexagons)}

    def _coordinate_in_restriction(self, coordinate: Point) -> bool:
        if self.restriction is None:
            return True
        return self.restriction.contains(coordinate)

    def _hexagon_in_restriction(self, hexagon: Hex) -> bool:
        return True


class LandMassesDataset:
    """
    Exposes the Natural Earth Land dataset at a scale of 10 meters. You can use
    this class to query whether a coordinate is on a land mass or on water.

    https://www.naturalearthdata.com/downloads/10m-physical-vectors/10m-land/
    """
    _landmasses: Optional[MultiPolygon]
    _restriction: Optional[Polygon]

    def __init__(self, restriction: Optional[Polygon] = None) -> None:
        self._landmasses = None
        self._restriction = restriction
        speedups.enable()

    def coordinate_on_land(self, coordinate: Point) -> bool:
        """
        Returns whether the given point is on water or land. The coordinate
        must the given in the `epsg:4326` (WGS 84) coordinate reference system.
        """
        self._read_data()
        return self._landmasses.contains(coordinate)

    def hexagon_on_land(self, hexagon: Hex, layout: Layout) -> bool:
        """
        Returns whether the given hexagon is completely on land or not.
        """
        self._read_data()
        polygon = layout.polygon(hexagon)
        return self._landmasses.covers(polygon)

    def plot(self, *args, **kwargs):
        self._read_data()
        landmasses = geopandas.GeoSeries(self._landmasses)
        return landmasses.plot(*args, **kwargs)

    def _read_data(self) -> None:
        if self._landmasses is not None:
            return

        landmasses = geopandas.read_file(
            Path(__file__).parent / '..' / 'data' / 'ne_10m_land.zip')
        if self._restriction is not None:
            landmasses = geopandas.clip(landmasses, self._restriction)
        self._landmasses = landmasses.unary_union


class HexagonalOceanWorld(HexagonalWorld):
    _land_only_in: MultiPolygon
    _landmasses: LandMassesDataset

    def __init__(self, layout: WorldLayout, restriction: Optional[Polygon], *, land_only_in: Optional[MultiPolygon] = None) -> None:
        self._land_only_in = land_only_in

        # We require land masses data for more than the original restriction of the hexagonal
        # world. Otherwise we would have hexagons all around the border, as the hexagons on the
        # border reach over the original restriction and would thus not be covered by land.
        if restriction is None:
            self._landmasses = LandMassesDataset()
        else:
            bounds = box(*restriction.bounds)
            self._landmasses = LandMassesDataset(affinity.scale(bounds, 1.2, 1.2))

        super().__init__(layout, restriction=restriction)

    @classmethod
    def for_layout_parameters(cls, orientation: OrientationParam, size: Point,
                              restriction: Optional[Polygon] = None, *,
                              land_only_in: Optional[MultiPolygon] = None) -> 'HexagonalOceanWorld':
        orientation = Orientation.from_orientation_param(orientation)
        layout = WorldLayout(orientation, size, Point(0, 0))
        return cls(layout, restriction, land_only_in=land_only_in)

    def __hash__(self) -> int:
        # Make hash different from an instance of `HexagonalWorld` with the
        # exact same parameters
        return hash(1) ^ super().__hash__()

    def _hexagon_in_restriction(self, hexagon: Hex) -> bool:
        if self._landmasses.hexagon_on_land(hexagon, self.layout):
            return False
        return super()._hexagon_in_restriction(hexagon)
