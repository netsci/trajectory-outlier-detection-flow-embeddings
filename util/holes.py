from itertools import combinations
from typing import Tuple

import networkx as nx
import numpy as np

from util import SEED


def holes_overlapping(hole_centers: np.ndarray, semi_majors: np.ndarray, padding: float = 0.1) -> bool:
    """
    Returns whether any of the given holes (ellipses) overlap.

    Notice that this function does not take rotation into account. It assumes
    a worst-case scenario where any two ellipses are rotated such that their
    semi-major axes align.

    Parameters
    ----------
    hole_centers : np.ndarray of size n ⨉ 2
    semi_majors : np.ndarray of size n
    padding : float, default=0.1
        The space that should be guaranteed between any two holes.
    """
    for i, j in combinations(range(hole_centers.shape[0]), 2):
        if np.linalg.norm(hole_centers[i] - hole_centers[j]) <= semi_majors[i] + semi_majors[j] + padding:
            return True
    return False


def random_holes(number_holes: int, *, seed: SEED = None) -> Tuple[np.ndarray, np.ndarray, np.ndarray]:
    """
    Returns `number_holes` random holes (ellipses) in the two dimensional space
    [0, 1] ⨉ [0, 1].

    The holes are guaranteed to not overlap, i.e., there is at least one edge
    between any two holes when placing the holes into a random triangle graph.

    Holes are placed *inside* the space and do not touch the border. When
    placing the holes in a random triangle graph, they are completely
    surrounded by edges and are not connected with the outside face.
    """
    random_state = nx.utils.create_random_state(seed)

    while True:
        hole_centers = random_state.uniform(0.1, 0.9, (number_holes, 2))
        axis_values = np.sort(random_state.uniform(0.05, 0.4, (number_holes, 2)))

        if holes_overlapping(hole_centers, axis_values[:, 1]):
            # Found an overlap of two holes. Restart.
            continue

        if np.any(hole_centers[:, 0] - axis_values[:, 1] <= 0.1) or np.any(hole_centers[:, 0] + axis_values[:, 1] >= 0.9):
            continue
        if np.any(hole_centers[:, 1] - axis_values[:, 1] <= 0.1) or np.any(hole_centers[:, 1] + axis_values[:, 1] >= 0.9):
            continue

        # if we reach this, all conditions are met and we are fine with the random example
        hole_rotations = random_state.uniform(0, 180, number_holes)
        return hole_centers, axis_values, hole_rotations
